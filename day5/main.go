package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Pass string

func readInput(fname string) []Pass {
	f,err := os.Open(fname)
	if err!=nil {
		log.Fatal(err)
	}

	passes := []Pass{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		passes = append(passes, Pass(scanner.Text()))
	}
	return passes
}

func seatID(pass Pass) int {
	// basically convert to binary
	id := 0
	for _,c := range(pass) {
		id = id *2
		if c=='B' || c=='R' {
			id = id + 1
		}
	}

	return id
}

func part1(passes []Pass) int {
	max := 0

	for _,pass := range(passes) {
		id := seatID(pass)
		if id>max {
			max = id
		}
	}

	return max
}

func part2(passes []Pass) int {
	ids:= make([]bool, 1024)

	for _,pass := range(passes) {
		id := seatID(pass)
		ids[id] = true
	}

	for i:=1; i<1023; i++ {
		if ids[i-1] && !ids[i] && ids[i+1] {
			return i
		}
	}

	return -1
}

func main() {
	input := readInput("input.txt")
	fmt.Println("Day 5")

	// fmt.Println(seatID("BFFFBBFRRR"))
	// fmt.Println(seatID("FFFBBBFRRR"))
	// fmt.Println(seatID("BBFFBBFRLL"))

	fmt.Println("part 1", part1(input))
	fmt.Println("part 2", part2(input))
}


