package main

// I'm actually fairly happy with this code for a change. My only real
// bug was in my code that calculated the bounding box for cells I
// needed to consider for flipping:
//
// Some resources:
//
// hex coordinate system https://www.redblobgames.com/grids/hexagons/
// printable hex graph paper https://incompetech.com/graphpaper/hexagonal/

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Hex struct {
	x int
	y int
}

type Input struct {
	commands []Command
}

type Command []string

type Grid map[Hex]bool

func (h Hex) move(dir string) Hex {
	switch {
	case dir == "w":
		return Hex{h.x - 1, h.y}
	case dir == "e":
		return Hex{h.x + 1, h.y}
	case dir == "nw":
		return Hex{h.x, h.y - 1}
	case dir == "ne":
		return Hex{h.x + 1, h.y - 1}
	case dir == "sw":
		return Hex{h.x - 1, h.y + 1}
	case dir == "se":
		return Hex{h.x, h.y + 1}
	}

	log.Fatal("unknown dir", dir)
	return Hex{}
}

func (h Hex) execute(cmd Command) Hex {
	for _, dir := range cmd {
		h = h.move(dir)
	}
	return h
}

func readCommand(line string) Command {
	directions := []string{}
	for i := 0; i < len(line); i++ {
		if line[i] == 'w' || line[i] == 'e' {
			directions = append(directions, line[i:i+1])
		} else {
			directions = append(directions, line[i:i+2])
			i++
		}
	}
	return directions
}

func readInput(fname string) Input {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	input := Input{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if scanner.Text() != "" {
			command := readCommand(scanner.Text())
			input.commands = append(input.commands, command)
		}
	}

	return input
}

func EmptyGrid() Grid {
	return map[Hex]bool{}
}

func NewGrid(input Input) Grid {
	grid := EmptyGrid()

	zero := Hex{0, 0}
	for _, cmd := range input.commands {
		point := zero.execute(cmd)
		if grid[point] {
			delete(grid, point)
		} else {
			grid[point] = true
		}
	}

	return grid
}

func (g Grid) countFlipped() int {
	return len(g)
}

func (g Grid) box() (int, int, int, int) {
	xmin, xmax, ymin, ymax := 0, 0, 0, 0

	for p := range g {
		if p.x < xmin {
			xmin = p.x
		}
		if p.x > xmax {
			xmax = p.x
		}
		if p.y < ymin {
			ymin = p.y
		}
		if p.y > ymax {
			ymax = p.y
		}
	}

	return xmin - 1, xmax + 1, ymin - 1, ymax + 1
}

func (g Grid) score(p Hex) int {
	count := 0
	for _, dir := range []string{"nw", "ne", "sw", "se", "w", "e"} {
		if g[p.move(dir)] {
			count++
		}
	}
	return count
}

func (g Grid) nextDay() Grid {
	next := EmptyGrid()

	xmin, xmax, ymin, ymax := g.box()

	for x := xmin; x <= xmax; x++ {
		for y := ymin; y <= ymax; y++ {
			p := Hex{x, y}
			n := g.score(p)

			if g[p] {
				// Any black tile with zero or more than 2 black tiles
				// immediately adjacent to it is flipped to white.
				if n == 1 || n == 2 {
					next[p] = true
				}
			} else {
				// Any white tile with exactly 2 black tiles immediately
				// adjacent to it is flipped to black.
				if n == 2 {
					next[p] = true
				}
			}
		}
	}

	return next
}

// ----------------------------------------

func part1(input Input) int {
	grid := NewGrid(input)

	return grid.countFlipped()
}

func part2(input Input) int {
	grid := NewGrid(input)

	for i := 1; i <= 100; i++ {
		grid = grid.nextDay()
	}

	return grid.countFlipped()
}

// ----------------------------------------

func main() {
	//input := readInput("sample.txt")
	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("Day 24")
	fmt.Println("part 1: ", part1(input))
	fmt.Println("part 2: ", part2(input))
}
