package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func readInput(fname string) []int64 {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	numbers := []int64{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		n, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		numbers = append(numbers, n)
	}
	return numbers
}

// ----------------------------------------

func valid(nums []int64, n int) bool {
	// valid if there is some pair in the prior
	// 25 numbers that sums to the number at place n
	for i := 1; i <= 25; i++ {
		for j := i + 1; j <= 25; j++ {
			if nums[n-i]+nums[n-j] == nums[n] {
				return true
			}
		}
	}
	return false
}

func part1(input []int64) int64 {
	// return the first invalid number after the preamble
	for i := 25; i < len(input); i++ {
		if !valid(input, i) {
			return input[i]
		}
	}
	return -1
}

// ----------------------------------------

func part2(input []int64) int64 {
	target := int64(177777905) // hardcoding part1 answer

	for i := 0; i < len(input); i++ {
		// for each position from beginning to end,
		// check if there is continuous sequence of numbers
		// that adds to the target

		sum := input[i]
		min := input[i]
		max := input[i]

		// keeping summing the numbers from i forward until
		// we reach or exceed the sum
		for j := i + 1; j < len(input) && sum < target; j++ {
			sum += input[j]

			// optimistically compute the min/max
			// so we'll have it if we hit the target
			if input[j] < min {
				min = input[j]
			}
			if input[j] > max {
				max = input[j]
			}
		}

		if sum == target {
			// we landed on the sum exactly, victory!
			return min + max
		}
	}

	return -1
}

// ----------------------------------------

func main() {
	input := readInput("input.txt")
	fmt.Println("Day 9")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
