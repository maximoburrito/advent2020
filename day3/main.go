package main

// https://adventofcode.com/2020/day/2

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// the datastructures are a little un
type Map struct {
	Lines []MapLine
}

type MapLine []bool

// parse lines. each map location is a boolean indicating if their is a tree or not
func parseLine(line string) MapLine {
	row := make([]bool, len(line))

	for i, c := range line {
		row[i] = c == '#'
	}

	return row
}

func readInput() Map {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	m := Map{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := parseLine(scanner.Text())
		m.Lines = append(m.Lines, line)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return m
}

// ----------------------------------------

func part1(m Map) int {
	nhit := 0

	x := 3
	for y := 1; y < len(m.Lines); y++ {
		if m.Lines[y][x] {
			nhit++
		}

		x = (x + 3) % len(m.Lines[y])
	}

	return nhit
}

// ----------------------------------------

func checkHits(m Map, dx int, dy int) int {
	nhit := 0
	x, y := dx, dy

	// walk the lines, moving dx, dy each step
	for y < len(m.Lines) {
		if m.Lines[y][x] {
			nhit++
		}

		x = (x + dx) % len(m.Lines[y])
		y += dy
	}

	return nhit
}

func part2(m Map) int {
	return checkHits(m, 1, 1) *
		checkHits(m, 3, 1) *
		checkHits(m, 5, 1) *
		checkHits(m, 7, 1) *
		checkHits(m, 1, 2)
}

// ----------------------------------------
func main() {
	input := readInput()

	fmt.Println("Day 3")
	fmt.Println("part 1", part1(input))
	fmt.Println("part 2", part2(input))
}
