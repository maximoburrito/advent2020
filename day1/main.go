package main

// https://adventofcode.com/2020/day/1

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func readInput() []int {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	nums := []int{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		n, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, n)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return nums
}

// My clojure instinct here is to toss all the numbers into a set
// and check for pairs by set membership. Instead I thought I might
// try a different clever solution of looking through pairs thinking
// that might pay off in the second part of the challenge. Of course,
// such optimizations are really overkill on day1, which is traditionally
// very easy

func part1(ns []int) int {
	nums := make([]int, len(ns))
	copy(nums, ns)

	sort.Ints(nums)

	i, j := 0, len(nums)-1

	for {
		if i == j {
			log.Fatal("no sum found")
		}

		sum := nums[i] + nums[j]
		if sum == 2020 {
			return nums[i] * nums[j]
		}

		if sum > 2020 {
			j--
		} else {
			i++
		}
	}
}

// I didn't see any way to adapt my solution to part1, so
// I went with nested for loops with indexes such that we'd never
// test the same i,j,k values twice.
// If the array were sorted as in part1, I could do even better, but
// that wasn't necessary

func part2(nums []int) int {
	for i := 0; i < len(nums)-2; i++ {
		for j := i + 1; j < len(nums)-1; j++ {
			// skip the innermost loop if the
			// partial sum of the outer loop is
			// already high enough
			if nums[i]+nums[j] >= 2020 {
				continue
			}

			for k := j + 1; k < len(nums); k++ {
				sum := nums[i] + nums[j] + nums[k]
				if sum == 2020 {
					return nums[i] * nums[j] * nums[k]
				}

			}
		}
	}

	log.Fatal("not found")
	return -1
}

func main() {
	nums := readInput()

	fmt.Println("Day 1")
	fmt.Printf("part1: %d\n", part1(nums))
	fmt.Printf("part2: %d\n", part2(nums))
}
