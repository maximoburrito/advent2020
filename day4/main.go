package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Record map[string]string

func (record *Record) addLine(line string) {
	parts := strings.Split(line, " ")
	for _, part := range parts {
		kv := strings.Split(part, ":")
		(*record)[kv[0]] = kv[1]
	}
}

func (record *Record) hasKey(k string) bool {
	_, found := (*record)[k]

	return found
}

func readInput(fname string) []Record {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	records := []Record{}
	record := Record{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			records = append(records, record)
			record = Record{}
		} else {
			record.addLine(line)
		}
	}
	records = append(records, record)

	return records
}

// ----------------------------------------
var requiredKeys = []string{
	"byr", // (Birth Year)
	"iyr", // (Issue Year)
	"eyr", // (Expiration Year)
	"hgt", // (Height)
	"hcl", // (Hair Color)
	"ecl", // (Eye Color)
	"pid", // (Passport ID)
	//"cid", // (Country ID)
}

func (record *Record) valid1() bool {
	for _, k := range requiredKeys {
		if !(*record).hasKey(k) {
			return false
		}
	}

	return true
}

func part1(input []Record) int {
	count := 0

	for _, record := range input {
		if record.valid1() {
			count++
		}
	}

	return count
}

// ----------------------------------------
func parseHeight(str string) (int, string) {
	splitAt := len(str) - 2

	metric := str[splitAt:]
	hgt, _ := strconv.Atoi(str[0:splitAt]) // ignore err

	return hgt, metric
}

func (record *Record) valid2() bool {
	//byr (Birth Year) - four digits; at least 1920 and at most 2002.
	byr, err := strconv.Atoi((*record)["byr"])
	if err != nil || byr < 1920 || byr > 2002 {
		return false
	}

	//iyr (Issue Year) - four digits; at least 2010 and at most 2020.
	iyr, err := strconv.Atoi((*record)["iyr"])
	if err != nil || iyr < 2010 || iyr > 2020 {
		return false
	}

	//eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
	eyr, err := strconv.Atoi((*record)["eyr"])
	if err != nil || eyr < 2020 || eyr > 2030 {
		return false
	}

	//hgt (Height) - a number followed by either cm or in:
	//  If cm, the number must be at least 150 and at most 193.
	//  If in, the number must be at least 59 and at most 76.
	hstr := (*record)["hgt"]
	if len(hstr) < 3 {
		return false
	}
	hgt, hmetric := parseHeight(hstr)
	if hmetric == "cm" {
		if hgt < 150 || hgt > 193 {
			return false
		}
	} else if hmetric == "in" {
		if hgt < 59 || hgt > 76 {
			return false
		}
	} else {
		return false
	}

	//hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
	hcl := (*record)["hcl"]
	match, err := regexp.MatchString(`^#[0-9a-f]{6}$`, hcl)
	if err != nil || !match {
		return false
	}

	//ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
	ecl := (*record)["ecl"]
	match, err = regexp.MatchString(`^(amb|blu|brn|gry|grn|hzl|oth)$`, ecl)
	if err != nil || !match {
		return false
	}

	//pid (Passport ID) - a nine-digit number, including leading zeroes.
	pid := (*record)["pid"]
	match, err = regexp.MatchString(`^\d{9}$`, pid)
	if err != nil || !match {
		return false
	}

	//cid (Country ID) - ignored, missing or not.

	return true
}

func part2(input []Record) int {
	count := 0

	for _, record := range input {
		if record.valid2() {
			count++
		}
	}

	return count
}

// ----------------------------------------

func main() {
	input := readInput("input.txt")

	fmt.Println("Day 4")
	fmt.Println("part1", part1(input))
	fmt.Println("part2", part2(input))
}
