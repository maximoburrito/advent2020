package main

// today's solution looks a lot more like what I'd write in Clojure, and I think
// it really paid off. On the previous game of life problem, I used arrays and
// the code was really tedious and ugly. In this case, I've used a map with
// co-ordinates as the key. (In clj, I'd use a set, but I actually don't yet know how
// to do a set in golang) I'm honestly not sure how golang does struct
// comparisons, but they seem to work fine as map keys. (I had to write some test
// code at the beginning to verify) I'll need to to spend some time in go tutorials
// to truly understand this. What I love about this code is that the bounds checking
// is extremely easy, and it was trivial to add a fourth dimension to the position
// struct.

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type Input map[Position]bool

type Position struct {
	x int
	y int
	z int
	w int
}

type MinMax struct {
	min int
	max int
}

type Bounds struct {
	x MinMax
	y MinMax
	z MinMax
	w MinMax
}

func readInput(fname string) Input {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatal(err)
	}

	input := map[Position]bool{}

	for y, line := range strings.Split(string(data), "\n") {
		for x, c := range line {
			if c == '#' {
				input[Position{x, y, 0, 0}] = true
			}
		}
	}

	return input
}

func bounds(input Input) Bounds {
	b := Bounds{}

	b.x.min = 10000 // where is max/min int?
	b.y.min = 10000 // probably should have just used
	b.z.min = 10000 // input[0] values and not this
	b.w.min = 10000

	b.x.max = -10000
	b.y.max = -10000
	b.z.max = -10000
	b.w.max = -10000

	for k := range input {
		if k.x < b.x.min {
			b.x.min = k.x
		}
		if k.x > b.x.max {
			b.x.max = k.x
		}
		if k.y < b.y.min {
			b.y.min = k.y
		}
		if k.y > b.y.max {
			b.y.max = k.y
		}
		if k.z < b.z.min {
			b.z.min = k.z
		}
		if k.z > b.z.max {
			b.z.max = k.z
		}
		if k.w < b.w.min {
			b.w.min = k.w
		}
		if k.w > b.w.max {
			b.w.max = k.w
		}
	}

	return b
}

// ----------------------------------------

func nextVal3D(input Input, p Position) bool {
	var total int

	for _, dx := range []int{-1, 0, 1} {
		for _, dy := range []int{-1, 0, 1} {
			for _, dz := range []int{-1, 0, 1} {
				// always use w=0 for 3d
				newp := Position{p.x + dx, p.y + dy, p.z + dz, 0}
				if p == newp {
					continue
				}
				if input[newp] {
					total++
				}
			}
		}
	}

	if input[p] {
		if (total == 2) || (total == 3) {
			return true
		}
	} else {
		if total == 3 {
			return true
		}
	}

	return false
}

func step3D(input Input) Input {
	b := bounds(input)

	next := Input{}
	for x := b.x.min - 1; x <= b.x.max+1; x++ {
		for y := b.y.min - 1; y <= b.y.max+1; y++ {
			for z := b.z.min - 1; z <= b.z.max+1; z++ {
				p := Position{x, y, z, 0} // w=0
				if nextVal3D(input, p) {
					next[p] = true
				}
			}
		}
	}

	//	fmt.Println("STEP", len(input), "->", len(next))
	return next
}

func part1(input Input) int {
	for i := 0; i < 6; i++ {
		input = step3D(input)
	}
	return len(input)
}

// ----------------------------------------

func nextVal4D(input Input, p Position) bool {
	var total int

	for _, dx := range []int{-1, 0, 1} {
		for _, dy := range []int{-1, 0, 1} {
			for _, dz := range []int{-1, 0, 1} {
				for _, dw := range []int{-1, 0, 1} {
					newp := Position{p.x + dx, p.y + dy, p.z + dz, p.w + dw}
					if p == newp {
						continue
					}
					if input[newp] {
						total++
					}
				}
			}
		}
	}

	if input[p] {
		if (total == 2) || (total == 3) {
			return true
		}
	} else {
		if total == 3 {
			return true
		}
	}

	return false
}

func step4D(input Input) Input {
	next := Input{}
	b := bounds(input)

	for x := b.x.min - 1; x <= b.x.max+1; x++ {
		for y := b.y.min - 1; y <= b.y.max+1; y++ {
			for z := b.z.min - 1; z <= b.z.max+1; z++ {
				for w := b.w.min - 1; w <= b.w.max+1; w++ {
					p := Position{x, y, z, w}
					if nextVal4D(input, p) {
						next[p] = true
					}
				}
			}
		}
	}

	//fmt.Println("STEP", len(input), "->", len(next))
	return next
}

func part2(input Input) int {
	for i := 0; i < 6; i++ {
		input = step4D(input)
	}
	return len(input)
}

// ----------------------------------------

func main() {
	fmt.Println("Day 17")
	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("Part 1", part1(input))
	fmt.Println("Part 2", part2(input))
}
