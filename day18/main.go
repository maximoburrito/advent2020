package main

// I hate this code so much

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type Input []string

type Stack struct {
	vals [][]string
}

func (s *Stack) pushLevel() {
	s.vals = append(s.vals, []string{})
}

func (s *Stack) pushVal(v string) {
	top := len(s.vals) - 1
	s.vals[top] = append(s.vals[top], v)
}

func (s *Stack) compute(plus bool, star bool) {
	old := s.vals[len(s.vals)-1]
	new := []string{}
	for _, val := range old {
		if len(new) == 0 {
			new = append(new, val)
			continue
		}

		topval := new[len(new)-1]
		if (topval == "+" && plus) || (topval == "*" && star) {
			n1 := new[len(new)-2]
			new = new[0 : len(new)-2]
			new = append(new, op(n1, topval, val))
		} else {
			new = append(new, val)
		}
	}
	s.vals[len(s.vals)-1] = new
}

func (s *Stack) popLevel() {
	top := len(s.vals) - 1
	if len(s.vals[top]) != 1 {
		log.Fatal("bad pop")
	}
	val := s.vals[top][0]
	s.vals = s.vals[0:top]
	s.pushVal(val)
}

func op(val1 string, op string, val2 string) string {
	n1, err := strconv.Atoi(val1)
	if err != nil {
		log.Fatal(err)
	}
	n2, err := strconv.Atoi(val2)
	if err != nil {
		log.Fatal(err)
	}

	if op == "*" {
		return fmt.Sprint(n1 * n2)
	} else if op == "+" {
		return fmt.Sprint(n1 + n2)
	}

	log.Fatal("unknown op")
	return ""
}

func eval(expr string, part2 bool) int {
	expr = strings.Replace(expr, "(", "( ", -1)
	expr = strings.Replace(expr, ")", " )", -1)
	scanner := bufio.NewScanner(strings.NewReader(expr))
	scanner.Split(bufio.ScanWords)

	stack := Stack{}
	stack.pushLevel()
	for scanner.Scan() {
		text := scanner.Text()
		if text == "(" {
			stack.pushLevel()
		} else if text == ")" {
			if part2 {
				stack.compute(true, false)
				//fmt.Println("!", stack)
				stack.compute(false, true)
				//fmt.Println("!", stack)

			} else {
				stack.compute(true, true)
			}
			//fmt.Println("!", stack)
			stack.popLevel()
		} else {
			stack.pushVal(text)
		}

		//fmt.Println("::::", text, "->", stack)
	}

	if part2 {
		stack.compute(true, false)
		stack.compute(false, true)
	} else {
		stack.compute(true, true)
	}

	n, err := strconv.Atoi(stack.vals[0][0])
	if err != nil {
		log.Fatal(err)
	}
	return n
}

// ----------------------------------------
func readInput(fname string) Input {
	bytes, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatal(err)
	}

	return strings.Split(strings.TrimSpace(string(bytes)), "\n")
}

// ----------------------------------------
func simpleTest() {
	fmt.Println(eval("1 + (2 * 3) + (4 * (5 + 6))", false))
	fmt.Println(eval("2 * 3 + (4 * 5)", false))                                 //26
	fmt.Println(eval("5 + (8 * 3 + 9 + 3 * 4 * 3)", false))                     // 437
	fmt.Println(eval("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", false))       //12240
	fmt.Println(eval("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", false)) // 13632

	fmt.Println(eval("1 + (2 * 3) + (4 * (5 + 6))", true)) // 51
	fmt.Println(eval("2 * 3 + (4 * 5)", true))
	fmt.Println(eval("5 + (8 * 3 + 9 + 3 * 4 * 3)", true)) // 1445
	fmt.Println(eval("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", true))
	fmt.Println(eval("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", true))
}

func part1(input Input) int {
	total := 0
	for _, expr := range input {
		total += eval(expr, false)
	}
	return total
}

func part2(input Input) int {
	total := 0
	for _, expr := range input {
		total += eval(expr, true)
	}
	return total
}

func main() {
	fmt.Println("Day 18")
	//simpleTest()

	input := readInput("input.txt")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
