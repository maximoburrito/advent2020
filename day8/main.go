package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type Program []Instruction

type Instruction struct {
	OpCode string
	Offset int
}

type Execution struct {
	Code Program
	PC   int
	ACC  int
}

func readInstruction(line string) Instruction {
	re := regexp.MustCompile(`(...) ([+-]\d+)`)

	match := re.FindStringSubmatch(line)
	if len(match) != 3 {
		log.Fatal("can't parse", line)
	}

	offset, err := strconv.Atoi(match[2])
	if err != nil {
		log.Fatal(err)
	}
	instruction := Instruction{match[1], offset}
	return instruction
}

func readInput(fname string) Program {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	program := Program{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		instruction := readInstruction(scanner.Text())
		program = append(program, instruction)
	}

	return program
}

func (state *Execution) step() {
	// perform one program step in the execution
	instruction := state.Code[state.PC]
	if instruction.OpCode == "nop" {
		state.PC++
	} else if instruction.OpCode == "acc" {
		state.ACC += instruction.Offset
		state.PC++
	} else if instruction.OpCode == "jmp" {
		state.PC += instruction.Offset
	} else {
		log.Fatal("unknown opcode", instruction)
	}
}

func (state *Execution) run() bool {
	// run the program until it terminates
	// returning error true when the program halts due to a loop
	infiniteloop := false

	pchit := make([]bool, len(state.Code))
	for {
		if state.PC >= len(pchit) {
			//fmt.Println("TERMINATE!")
			break
		}
		if pchit[state.PC] {
			//fmt.Println("LOOP")
			infiniteloop = true
			break
		}
		pchit[state.PC] = true
		state.step()
		//fmt.Println(state.PC, state.ACC)
	}

	return infiniteloop
}

// ----------------------------------------

func part1(program Program) int {
	// run the program once, and return the accumulator
	state := Execution{program, 0, 0}
	state.run()
	return state.ACC
}

// ----------------------------------------
func swapOpCode(opcode string) string {
	// jmp->nop, nop->jmp
	if opcode == `jmp` {
		return `nop`
	} else if opcode == `nop` {
		return `jmp`
	} else {
		return opcode
	}
}

func part2(program Program) int {
	// run the program N times, each time swapping the opcode at location N
	for i := 0; i < len(program); i++ {
		// make a copy of the program with a (maybe) changed instruction
		newprogram := make([]Instruction, len(program))
		copy(newprogram, program)
		newprogram[i].OpCode = swapOpCode(newprogram[i].OpCode)

		state := Execution{newprogram, 0, 0}
		looperr := state.run()
		if !looperr {
			// stop when we don't loop
			return state.ACC
		}
	}

	return -1
}

func main() {
	//program := readInput("sample.txt")
	program := readInput("input.txt")
	fmt.Println("Day 8")
	fmt.Println("part1", part1(program))
	fmt.Println("part2", part2(program))
}
