package main

// This one is pretty ugly. I'm clearly struggling
// with the various integer types, casting and bit manipulation.
//
// I'm also not sure how to create an array of different typed
// things. For example, here an Instruction might be as Mask
// instruction or a Mem instruction. I want to iterate through them
// and do some work based on the actual type.  In clojure, I'd have an
// a vector of maps, with each map containting a type field. In go, I
// ended up creating a struct with two pointers. I'm not satisfied with
// this, but it works so... I'm looking forward to seeing other implementations
// for ideas on how to do this better.
//
// I've also left in some of my debugging comments instead of stripping them out.

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type Input []Instruction
type Instruction struct {
	mask *Mask
	mem  *Mem
}

type Mask struct {
	x    uint64
	one  uint64
	zero uint64
}

type Mem struct {
	loc   int
	value uint64
}

type Memory map[int]uint64

func sum(memory Memory) int {
	sum := 0
	for _, val := range memory {
		sum += int(val)
	}
	return sum
}

func readMask(line string) *Mask {
	maskre := regexp.MustCompile(`mask = ([01X]+)`)
	match := maskre.FindStringSubmatch(line)
	if len(match) == 2 {
		var mask Mask

		for _, c := range match[1] {
			mask.x *= 2
			mask.one *= 2
			mask.zero *= 2

			if c == '1' {
				mask.one |= 1
			}

			if c == '0' {
				mask.zero |= 1
			}

			if c == 'X' {
				mask.x |= 1
			}
		}
		return &mask
	}
	return nil
}

func readMem(line string) *Mem {
	memre := regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
	match := memre.FindStringSubmatch(line)
	if len(match) == 3 {
		var mem Mem
		n, err := strconv.Atoi(match[1])
		if err != nil {
			log.Fatal(err)
		}
		mem.loc = n

		n, err = strconv.Atoi(match[2])
		if err != nil {
			log.Fatal(err)
		}
		mem.value = uint64(n)
		return &mem
	}
	return nil
}

func readLine(line string) Instruction {
	instruction := Instruction{}

	instruction.mask = readMask(line)
	instruction.mem = readMem(line)

	if instruction.mask == nil && instruction.mem == nil {
		log.Fatal("unabled to parse: ", line)
	}

	return instruction
}

func readInput(fname string) Input {
	instructions := Input{}

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		instructions = append(instructions, readLine(scanner.Text()))
	}

	return instructions
}

// ----------------------------------------

func part1(input Input) int {
	memory := Memory{}

	var mask Mask
	for _, instruction := range input {
		if instruction.mask != nil {
			mask = *instruction.mask
		} else {
			wval := (instruction.mem.value & mask.x) | mask.one
			memory[instruction.mem.loc] = wval
		}
	}

	return sum(memory)
}

// ----------------------------------------

func applyMask(m Mask, base int, bits int) int {
	addr := uint64(0)
	countx := uint64(1)

	for i := 0; i < 36; i++ {
		bit := uint64(1 << i)
		if m.one&bit != 0 {
			addr |= bit
		} else if m.zero&bit != 0 {
			if uint64(base)&bit != 0 {
				addr |= bit
			}
		} else if m.x&bit != 0 {
			if countx&uint64(bits) != 0 {
				addr |= bit
			}
			countx = countx << 1
		}
		//fmt.Printf(" -- %38s\n", strconv.FormatUint(addr,2))
	}

	//fmt.Println(" -- ", strconv.FormatUint(addr,2))
	return int(addr)
}

func setMemory(memory *Memory, mask Mask, mem Mem) {
	nx := 0
	for i := 0; i <= 36; i++ {
		if mask.x&(1<<i) != 0 {
			nx++
		}
	}

	// fmt.Printf("SET* nx=%d, mem=%v\n", nx, mem)
	// fmt.Printf(" 0  %038s\n", strconv.FormatUint(mask.zero,2))
	// fmt.Printf(" 1  %038s\n", strconv.FormatUint(mask.one,2))
	// fmt.Printf(" X  %038s\n", strconv.FormatUint(mask.x,2))

	// fmt.Printf(" _  %038s\n", strconv.FormatUint(uint64(mem.loc),2))

	for i := 0; i < (1 << nx); i++ {
		loc := applyMask(mask, mem.loc, i)
		//fmt.Printf(" --> [%d]=%d\n", loc, mem.value)
		(*memory)[loc] = mem.value
	}
}

func part2(input Input) int {
	memory := Memory{}

	var mask Mask
	for _, instruction := range input {
		// fmt.Println("X", instruction)
		if instruction.mask != nil {
			mask = *instruction.mask
			continue
		}

		setMemory(&memory, mask, *instruction.mem)
		//fmt.Println("M", memory)
	}

	return sum(memory)
}

// ----------------------------------------
func main() {
	fmt.Println("Day 14")

	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
