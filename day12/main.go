package main

// I'm trying a few slight style changes today based on other AoC go
// code I've seen. I guess we'll find out tomorrow is any of this
// sticks...

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

type Input []Command
type Command struct {
	Action string
	Number int
}

func parseLine(line string) Command {
	var cmd Command

	cmd.Action = line[0:1]

	n, err := strconv.Atoi(line[1:])
	if err != nil {
		log.Fatal(err)
	}
	cmd.Number = n

	return cmd
}

func readInput(fname string) Input {
	var input Input

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		input = append(input, parseLine(scanner.Text()))
	}

	return input
}

// there's no int abs?
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// ----------------------------------------

type State1 struct {
	x   int
	y   int
	dir int // n/e/s/w 0/1/2/3 -- do we have enums?
}

func execute(cmd Command, state State1) State1 {
	if cmd.Action == "F" {
		if state.dir == 0 {
			return execute(Command{"N", cmd.Number}, state)
		} else if state.dir == 1 {
			return execute(Command{"E", cmd.Number}, state)
		} else if state.dir == 2 {
			return execute(Command{"S", cmd.Number}, state)
		} else if state.dir == 3 {
			return execute(Command{"W", cmd.Number}, state)
		}
	} else if cmd.Action == "N" {
		return State1{state.x, state.y + cmd.Number, state.dir}
	} else if cmd.Action == "E" {
		return State1{state.x + cmd.Number, state.y, state.dir}
	} else if cmd.Action == "S" {
		return State1{state.x, state.y - cmd.Number, state.dir}
	} else if cmd.Action == "W" {
		return State1{state.x - cmd.Number, state.y, state.dir}
	} else if cmd.Action == "R" {
		dir := (state.dir + int(cmd.Number/90)) % 4
		return State1{state.x, state.y, dir}
	} else if cmd.Action == "L" {
		// golang mod can return negative numbers? weird...
		dir := (state.dir - int(cmd.Number/90) + 4) % 4
		return State1{state.x, state.y, dir}
	}

	log.Fatal("invalid command", cmd, state)
	return state
}

func part1(input Input) int {
	state := State1{0, 0, 1}

	for _, cmd := range input {
		state = execute(cmd, state)
	}

	return abs(state.x) + abs(state.y)
}

// ----------------------------------------

type State2 struct {
	x  int
	y  int
	wx int
	wy int
}

func execute2(cmd Command, state State2) State2 {
	if cmd.Action == "F" {
		return State2{
			state.x + state.wx*cmd.Number,
			state.y + state.wy*cmd.Number,
			state.wx,
			state.wy,
		}
	} else if cmd.Action == "N" {
		return State2{
			state.x,
			state.y,
			state.wx,
			state.wy + cmd.Number,
		}
	} else if cmd.Action == "E" {
		return State2{
			state.x,
			state.y,
			state.wx + cmd.Number,
			state.wy,
		}
	} else if cmd.Action == "S" {
		return State2{
			state.x,
			state.y,
			state.wx,
			state.wy - cmd.Number,
		}
	} else if cmd.Action == "W" {
		return State2{
			state.x,
			state.y,
			state.wx - cmd.Number,
			state.wy,
		}
	} else if cmd.Action == "R" {
		next := state // copy
		for i := 0; i < cmd.Number; i += 90 {
			next.wx, next.wy = next.wy, -next.wx
		}
		return next
	} else if cmd.Action == "L" {
		// rewriting this in terms of "R"
		return execute2(Command{"R", 360 - cmd.Number}, state)
	}

	log.Fatal("invalid command", cmd)
	return state
}

func part2(input Input) int {
	state := State2{0, 0, 10, 1}

	for _, cmd := range input {
		state = execute2(cmd, state)
	}

	return abs(state.x) + abs(state.y)
}

// ----------------------------------------
func main() {
	fmt.Println("Day 12")

	for _,fname := range([]string{"sample.txt", "input.txt"}) {
		fmt.Println("==", fname)

		input := readInput(fname)
		//fmt.Println(input)

		fmt.Println("part 1", part1(input))
		fmt.Println("part 2", part2(input))
	}
}
