package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// the votes of one person, from a to z
type Vote [26]bool

// the votes of a group of people
type VoteGroup []Vote

// all the votes in the input, by group
type Votes []VoteGroup

// record the votes from a person, probably better as a constructor.
func (v *Vote) record(line string) {
	for _, c := range line {
		v[int(c)-int('a')] = true
	}
}

// get the vote total for a vote
func (v *Vote) sum() int {
	count := 0
	for _, yesno := range v {
		if yesno {
			count++
		}
	}
	return count
}

func (g *VoteGroup) mergeOr() *Vote {
	merged := Vote{}
	for _, v := range *g {
		for i, yesno := range v {
			merged[i] = merged[i] || yesno
		}
	}

	return &merged
}

func (g *VoteGroup) mergeAnd() *Vote {
	merged := (*g)[0]
	for _, v := range *g {
		for i, yesno := range v {
			merged[i] = merged[i] && yesno
		}
	}

	return &merged
}

func readInput(filename string) Votes {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	votes := Votes{}
	group := VoteGroup{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			votes = append(votes, group)
			group = VoteGroup{}
		} else {
			vote := Vote{}
			vote.record(line)
			group = append(group, vote)
		}
	}
	votes = append(votes, group)

	return votes
}

// ----------------------------------------

func part1(votes Votes) int {
	total := 0
	for _, group := range votes {
		total += group.mergeOr().sum()
	}
	return total
}

// ----------------------------------------

func part2(votes Votes) int {
	total := 0
	for _, group := range votes {
		total += group.mergeAnd().sum()
	}
	return total
}

// ----------------------------------------

func main() {
	votes := readInput("input.txt")

	fmt.Println("Day 6")
	fmt.Println("part 1", part1(votes))
	fmt.Println("part 2", part2(votes))
}
