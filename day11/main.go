package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const NOSEAT = '.'
const OCCUPIED = '#'
const EMPTY = 'L'

const SANITY_STOP = 1000

// keep the data structure simple today, but I wonder
// if an enum of some sort makes sense
type Seats [][]rune

func readInput(fname string) Seats {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	seats := [][]rune{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		seats = append(seats, []rune(scanner.Text()))
	}

	return seats
}

// ----------------------------------------
// common methods

// strictly for debug purpose, dump the seats the way
// they look in the problem statement
func dump(s Seats) {
	for i := 0; i < len(s); i++ {
		fmt.Println(string(s[i]))
	}
	fmt.Println()
}

// count all the occupied seats
func countOccupied(seats Seats) int {
	total := 0
	for i := 0; i < len(seats); i++ {
		for j := 0; j < len(seats[i]); j++ {
			if seats[i][j] == OCCUPIED {
				total++
			}
		}
	}
	return total
}

// compare two Seats - maybe should just use DeepEquals?
// it seems like I shouldn't need to write this
func sameSeats(a Seats, b Seats) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if len(a[i]) != len(b[i]) {
			return false
		}

		for j := 0; j < len(a[i]); j++ {
			if a[i][j] != b[i][j] {
				return false
			}
		}
	}

	return true
}

// return a copy of a Seats structure
// again, why am I writing this?
func copySeats(seats Seats) Seats {
	nextSeats := make([][]rune, len(seats))
	for i := 0; i < len(seats); i++ {
		nextSeats[i] = make([]rune, len(seats[i]))
		copy(nextSeats[i], seats[i])
	}

	return nextSeats
}

// ----------------------------------------
// part 1 only

// true if the seat is occupied, false in all other cases, including
// invalid seat index
func occupied1(seats Seats, row int, col int) bool {
	if row < 0 || row >= len(seats) || col < 0 || col >= len(seats[row]) {
		return false
	}

	return seats[row][col] == OCCUPIED
}

// for a given row, column, calculate the next value for that seat
func nextPart1(seats Seats, row int, col int) rune {
	if seats[row][col] == NOSEAT {
		return NOSEAT
	}

	// generate the 8 row/col deltas and test if those seats are occupied
	count := 0
	for _, dr := range []int{-1, 0, 1} {
		for _, dc := range []int{-1, 0, 1} {
			if (dr != 0 || dc != 0) && occupied1(seats, row+dr, col+dc) {
				count++
			}
		}
	}

	// next state depends on current value and the count of nearby occupied seats
	if seats[row][col] == OCCUPIED && count >= 4 {
		return EMPTY
	}
	if seats[row][col] == EMPTY && count == 0 {
		return OCCUPIED
	}

	return seats[row][col]
}

// perform one step of seat changes, returning the
// result in a new structure
func stepPart1(seats Seats) Seats {
	next := copySeats(seats)

	for i := 0; i < len(seats); i++ {
		for j := 0; j < len(seats[i]); j++ {
			next[i][j] = nextPart1(seats, i, j)
		}
	}
	return next
}

func part1(seats Seats) int {
	for i := 0; i < SANITY_STOP; i++ {
		//fmt.Println("step ", i);
		newseats := stepPart1(seats)

		if sameSeats(seats, newseats) {
			return countOccupied(seats)
		}

		seats = newseats
		//dump(seats)
	}

	return -1
}

// ----------------------------------------
// part 2 specific code

// we no longer want to ask if the seat is occupied but if the
// first seat in the direction (indicated by the delta) is occupied
func occupied2(seats Seats, r int, c int, dr int, dc int) bool {
	// we'll multiply the delta by an increasing factor
	// in a given direction, until we hit a space with a seat
	// or overrun the bounds
	for factor := 1; ; factor++ {
		row := r + dr*factor
		col := c + dc*factor

		if row < 0 || row >= len(seats) || col < 0 || col >= len(seats[row]) {
			return false // out of bounds
		}

		if seats[row][col] != NOSEAT {
			// we found a seat, check if it's occupied
			return seats[row][col] == OCCUPIED
		}

		//no seat, so continue
	}

	// ugly - what's the go way to express this?
	log.Fatal("should not get here")
	return false
}

func nextPart2(seats Seats, row int, col int) rune {
	if seats[row][col] == NOSEAT {
		return NOSEAT
	}

	count := 0
	for _, dr := range []int{-1, 0, 1} {
		for _, dc := range []int{-1, 0, 1} {
			// check each delta, but instead of doing the calculation
			// here, we pass the direction/delta in so that function
			// can figure it out
			if (dr != 0 || dc != 0) && occupied2(seats, row, col, dr, dc) {
				count++
			}
		}
	}

	// the tests, note this changed to 5
	if seats[row][col] == OCCUPIED && count >= 5 {
		return EMPTY
	}
	if seats[row][col] == EMPTY && count == 0 {
		return OCCUPIED
	}

	return seats[row][col]
}

func stepPart2(seats Seats) Seats {
	next := copySeats(seats)

	for i := 0; i < len(seats); i++ {
		for j := 0; j < len(seats[i]); j++ {
			next[i][j] = nextPart2(seats, i, j)
		}
	}
	return next
}

func part2(seats Seats) int {
	for i := 0; i < SANITY_STOP; i++ {
		newseats := stepPart2(seats)

		if sameSeats(seats, newseats) {
			return countOccupied(seats)
		}

		seats = newseats
	}

	return -1
}

// ----------------------------------------

func main() {
	fmt.Println("Day 11")

	input := readInput("input.txt")
	//dump(input)

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
