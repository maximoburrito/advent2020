package main

// "you only need to handle the rules you have" -- UGGH

import (
	"fmt"
)

func part1(input Input) int {
	total := 0
	for _, msg := range input.messages {
		n := input.rules[0].matches(input.rules, msg)
		if n == len(msg) {
			total++
		}
	}
	return total
}

func p2match(rules Rules, msg string) bool {
	// by inspection of the rules we should see some number of rule 42
	// followed by some number of rule 31, with more 42 than
	// 31. Assuming that 42 and 31 don't have any overlappign
	// patterns. just match those rules greedily. "you only need to
	// handle the rules you have"

	n42 := 0
	n31 := 0

	start := 0
	for {
		n := rules[42].matches(rules, msg[start:])
		if n == 0 {
			break
		}
		n42++
		start += n
	}

	for {
		n := rules[31].matches(rules, msg[start:])
		if n == 0 {
			break
		}
		n31++
		start += n
	}

	//fmt.Printf("42=%d 31=%d extra='%s'\n", n42, n31, msg[start:])
	return len(msg[start:]) == 0 && n42 > n31 && n31 > 0
}

func part2(input Input) int {
	total := 0
	for _, msg := range input.messages {
		if p2match(input.rules, msg) {
			total++
		}
	}

	return total
}

// ----------------------------------------

func main() {
	fmt.Println("Day 19")

	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
