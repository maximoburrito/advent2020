package main

import (
	"fmt"
	"sort"
)

func dumpOrdered(rules Rules) {
	keys := []int{}
	for k := range rules {
		keys = append(keys, k)
	}

	sort.Ints(keys)

	for _, k := range keys {
		fmt.Printf("%3d: %v\n", k, rules[k])
	}
}
