package main

import "strings"

type Rules map[int]Rule

type Rule interface {
	matches(rules Rules, text string) int
	generate(rules Rules) []string
}

type OrRule struct {
	clauses []SimpleRule
}

type SimpleRule struct {
	rules []int
}

type TerminalRule struct {
	c string
}

// ========================================
func (r TerminalRule) matches(rules Rules, text string) int {
	if len(text) > 0 && strings.HasPrefix(text, r.c) {
		return len(r.c)
	}

	return 0
}

func (r OrRule) matches(rules Rules, text string) int {
	winner := 0
	for _, clause := range r.clauses {
		n := clause.matches(rules, text)
		if n > 0 {
			return n
		}
	}
	return winner
}

func (r SimpleRule) matches(rules Rules, text string) int {
	tot := 0
	for _, nrule := range r.rules {
		n := rules[nrule].matches(rules, text)
		if n == 0 {
			return 0
		}
		tot += n
		text = text[n:]
	}
	return tot
}
