package main

func (r TerminalRule) generate(rules Rules) []string {
	return []string{r.c}
}

func (r SimpleRule) generate(rules Rules) []string {
	gen := []string{""}

	for _, n := range r.rules {
		next := []string{}
		for _, prefix := range gen {
			for _, suffix := range rules[n].generate(rules) {
				next = append(next, prefix+suffix)
			}
		}
		gen = next
	}

	return gen
}

func (r OrRule) generate(rules Rules) []string {
	parts := []string{}
	for _, clause := range r.clauses {
		for _, part := range clause.generate(rules) {
			parts = append(parts, part)
		}
	}
	return parts
}
