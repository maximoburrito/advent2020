package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Input struct {
	rules    Rules
	messages []string
}

func mustInt(text string) int {
	n, err := strconv.Atoi(text)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

func mustInts(text string) []int {
	nums := []int{}
	for _, ntxt := range strings.Split(text, " ") {
		nums = append(nums, mustInt(ntxt))
	}
	return nums
}

func parseRule(line string) (int, Rule) {
	renum := regexp.MustCompile(`^(\d+): (.+)$`)
	match := renum.FindStringSubmatch(line)
	if len(match) != 3 {
		log.Fatal("can't parse", line)
	}
	num := mustInt(match[1])
	rulebody := match[2]

	reterminal := regexp.MustCompile(`^"(.)"$`)
	match = reterminal.FindStringSubmatch(rulebody)
	if len(match) == 2 {
		return num, TerminalRule{match[1]}
	}

	rule := OrRule{}
	for _, part := range strings.Split(rulebody, " | ") {
		rule.clauses = append(rule.clauses, SimpleRule{mustInts(part)})
	}

	return num, rule
}

func readInput(fname string) Input {
	input := Input{map[int]Rule{}, []string{}}

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if scanner.Text() == "" {
			break
		}
		n, rule := parseRule(scanner.Text())
		input.rules[n] = rule
	}

	for scanner.Scan() {
		input.messages = append(input.messages, scanner.Text())
	}

	return input
}
