package main

import (
	"fmt"
	"log"
)

type Input struct {
	tiles map[int]Tile
	size  int
	keys  []int
}

type Tile struct {
	points [][]rune
}

type TileOrientation struct {
	tile int
	rot  int // 0.. 7
}

const (
	UP    = 0
	DOWN  = 2
	LEFT  = 3
	RIGHT = 1
)

func actualDirection(rotation int, direction int) (int, bool) {
	switch {
	case rotation == 0: // no change
		switch {
		case direction == UP:
			return UP, false
		case direction == DOWN:
			return DOWN, false
		case direction == LEFT:
			return LEFT, false
		case direction == RIGHT:
			return RIGHT, false
		}
	case rotation == 1:
		switch {
		case direction == UP:
			return LEFT, true
		case direction == DOWN:
			return RIGHT, true
		case direction == LEFT:
			return DOWN, false
		case direction == RIGHT:
			return UP, false
		}
	case rotation == 2:
		switch {
		case direction == UP:
			return DOWN, true
		case direction == DOWN:
			return UP, true
		case direction == LEFT:
			return RIGHT, true
		case direction == RIGHT:
			return LEFT, true
		}
	case rotation == 3:
		switch {
		case direction == UP:
			return RIGHT, false
		case direction == DOWN:
			return LEFT, false
		case direction == LEFT:
			return UP, true
		case direction == RIGHT:
			return DOWN, true
		}
	case rotation == 4:
		switch {
		case direction == UP:
			return UP, true
		case direction == DOWN:
			return DOWN, true
		case direction == LEFT:
			return RIGHT, false
		case direction == RIGHT:
			return LEFT, false
		}
	case rotation == 5:
		switch {
		case direction == UP:
			return RIGHT, true
		case direction == DOWN:
			return LEFT, true
		case direction == LEFT:
			return DOWN, true
		case direction == RIGHT:
			return UP, true
		}
	case rotation == 6:
		switch {
		case direction == UP:
			return DOWN, false
		case direction == DOWN:
			return UP, false
		case direction == LEFT:
			return LEFT, true
		case direction == RIGHT:
			return RIGHT, true
		}
	case rotation == 7:
		switch {
		case direction == UP:
			return LEFT, false
		case direction == DOWN:
			return RIGHT, false
		case direction == LEFT:
			return UP, false
		case direction == RIGHT:
			return DOWN, false
		}
	}

	log.Fatal("unknown dir/rotation", rotation, direction)
	return -1, false
}

// binary revery 10 bits
func reverse(n int) int {
	result := 0

	for i := 0; i < 10; i++ {
		if n&(1<<i) != 0 {
			result |= 1 << (9 - i)
		}
	}

	return result
}

func (input Input) tileValue(which TileOrientation, direction int) int {
	actual, opposite := actualDirection(which.rot, direction)

	tile := input.tiles[input.keys[which.tile]]

	result := 0
	switch {
	case actual == UP:
		//fmt.Print("U ")
		for across := 0; across < len(tile.points); across++ {
			//fmt.Printf("%c", tile.points[0][across])
			if tile.points[0][across] == '#' {
				result |= 1 << across
			}
		}
		//fmt.Println()
		break

	case actual == DOWN:
		//fmt.Print("D ")
		for across := 0; across < len(tile.points); across++ {
			//fmt.Printf("%c", tile.points[len(tile.points)-1][across])
			if tile.points[len(tile.points)-1][across] == '#' {
				result |= 1 << across
			}
		}
		//fmt.Println()
		break
	case actual == LEFT:
		//fmt.Print(" ")
		for down := 0; down < len(tile.points); down++ {
			//fmt.Printf("%c", tile.points[down][0])
			if tile.points[down][0] == '#' {
				result |= 1 << down
			}
		}
		//fmt.Println()
		break
	case actual == RIGHT:
		for down := 0; down < len(tile.points); down++ {
			if tile.points[down][len(tile.points)-1] == '#' {
				result |= 1 << down
			}
		}
		break
	default:
		log.Fatal("unknown direction", actual)
	}

	if opposite {
		result = reverse(result)
	}

	//	fmt.Printf(" tileValue %v dir=%d actual=%d result=%d\n", which, direction, actual, result)
	return result
}

func (input Input) describeTile(k int) {
	fmt.Printf("Tile %d - id=%d\n", k, input.keys[k])
	fmt.Printf(" 0: up=%d down=%d left=%d right=%d\n",
		input.tileValue(TileOrientation{k, 0}, UP),
		input.tileValue(TileOrientation{k, 0}, DOWN),
		input.tileValue(TileOrientation{k, 0}, LEFT),
		input.tileValue(TileOrientation{k, 0}, RIGHT),
	)
	fmt.Printf(" -- up=%d down=%d left=%d right=%d\n",
		input.tileValue(TileOrientation{k, 4}, UP),
		input.tileValue(TileOrientation{k, 4}, DOWN),
		input.tileValue(TileOrientation{k, 6}, LEFT),
		input.tileValue(TileOrientation{k, 6}, RIGHT),
	)
}

func rotateRight(in [][]rune) [][]rune {
	dim := len(in)
	result := make([][]rune, dim)
	for i := 0; i < dim; i++ {
		result[i] = make([]rune, dim)
	}

	for i := 0; i < len(in); i++ {
		for j := 0; j < len(in[i]); j++ {
			result[j][dim-1-i] = in[i][j]
		}
	}
	return result
}
func rotate(in [][]rune, n int) [][]rune {
	result := in

	for z := 0; z < n; z++ {
		result = rotateRight(result)
	}

	return result
}

func flipx(in [][]rune) [][]rune {
	dim := len(in)
	result := make([][]rune, dim)
	for i := 0; i < dim; i++ {
		result[i] = make([]rune, dim)
	}

	for i := 0; i < len(in); i++ {
		for j := 0; j < len(in[i]); j++ {
			result[i][dim-1-j] = in[i][j]
		}
	}

	return result
}

func orient(in [][]rune, rot int) [][]rune {
	switch {
	case rot == 0:
		return in
	case rot == 1:
		return rotate(in, 1)
	case rot == 2:
		return rotate(in, 2)
	case rot == 3:
		return rotate(in, 3)
	case rot == 4:
		return flipx(in)
	case rot == 5:
		return flipx(rotate(in, 3))
	case rot == 6:
		return flipx(rotate(in, 2))
	case rot == 7:
		return flipx(rotate(in, 1))
	}

	log.Fatal("unknown rotation", rot)
	return nil
}

func cut(input Input, to TileOrientation) [][]rune {
	result := make([][]rune, 8)
	for i := 0; i < 8; i++ {
		result[i] = make([]rune, 8)
	}

	tile := input.tiles[input.keys[to.tile]]

	for i := 0; i < 8; i++ {
		for j := 0; j < 8; j++ {
			result[i][j] = tile.points[i+1][j+1]
		}
	}

	return orient(result, to.rot)
}

// ----------------------------------------
func NewInput() Input {
	return Input{
		map[int]Tile{},
		0,
		[]int{},
	}
}
