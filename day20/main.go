package main

// I learned a lot from this problem. Unfortuantely, I chose a poor initial strategy
// and spent most of my time debugging minor trivial mistakes. I could have surely
// saved a lot of hassle with some test cases.

import (
	"fmt"
	"log"
)

func nextSolution(input Input, to TileOrientation) (TileOrientation, bool) {
	to.rot++
	if to.rot <= 7 {
		return to, true
	}

	to.rot = 0

	to.tile++
	if to.tile < len(input.tiles) {
		return to, true
	}

	return to, false
}

func validPartial(input Input, solution []TileOrientation) bool {
	// check for unique tiles
	seen := map[int]bool{}
	for _, n := range solution {
		if seen[n.tile] {
			return false
		}
		seen[n.tile] = true
	}

	// check only the last tile
	n := len(solution) - 1

	across := n % input.size
	down := int(n / input.size)

	// check above
	if down > 0 {
		me := input.tileValue(solution[n], UP)
		other := input.tileValue(solution[n-input.size], DOWN)

		if me != other {
			return false
		}
	}

	//check left
	if across > 0 {
		me := input.tileValue(solution[n], LEFT)
		other := input.tileValue(solution[n-1], RIGHT)

		if me != other {
			return false
		}
	}

	return true
}

func solve(input Input) []TileOrientation {
	solution := []TileOrientation{}
	solution = append(solution, TileOrientation{})

	for len(solution) > 0 {
		// if we like the solution so far, advance the
		// solution to the next position to solve
		if validPartial(input, solution) {
			if len(solution) == len(input.tiles) {
				break
			}

			solution = append(solution, TileOrientation{})
			continue
		}

		// find the next solution, backtracking if needed
		for len(solution) > 0 {
			last := len(solution) - 1

			bump, ok := nextSolution(input, solution[last])
			if ok {
				solution[last] = bump
				break
			}

			solution = solution[0:last]
		}
	}

	if len(solution) == 0 {
		log.Fatal("no solution")
	}

	return solution
}

func part1(input Input) int {
	solution := solve(input)

	return input.keys[solution[0].tile] *
		input.keys[solution[input.size-1].tile] *
		input.keys[solution[input.size*input.size-input.size].tile] *
		input.keys[solution[input.size*input.size-1].tile]
}

func part2(input Input) int {
	world := make([][]rune, input.size*8)
	for i := 0; i < input.size*8; i++ {
		world[i] = make([]rune, input.size*8)
	}

	solution := solve(input)
	//	fmt.Println(solution)

	// for i := 0; i < len(solution); i++ {
	// 	fmt.Printf("%4d ", input.keys[solution[i].tile])
	// 	if (i+1)%input.size == 0 {
	// 		fmt.Println()
	// 	}
	// }

	for i := 0; i < input.size; i++ {
		for j := 0; j < input.size; j++ {
			sid := i*input.size + j

			tilepoints := cut(input, solution[sid])
			for x := 0; x < 8; x++ {
				for y := 0; y < 8; y++ {
					world[i*8+x][j*8+y] = tilepoints[x][y]
				}
			}
		}
	}

	return countMonsters(rotate(world, 2))
}

func pp(box [][]rune) {
	fmt.Println()
	for _, line := range box {
		fmt.Println(string(line))
	}
}

func main() {
	fmt.Println("Day 20")
	input := readInput("input.txt")

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
