package main

import (
	"bufio"
	"log"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
)

func readTile(scanner *bufio.Scanner) (int, *Tile) {
	if !scanner.Scan() {
		return -1, nil
	}

	re := regexp.MustCompile(`Tile (\d+):`)
	match := re.FindStringSubmatch(scanner.Text())
	if len(match) != 2 {
		log.Fatal("can't parse", scanner.Text())
	}
	n, err := strconv.Atoi(match[1])
	if err != nil {
		log.Fatal(err)
	}

	tile := Tile{[][]rune{}}
	for scanner.Scan() {
		if scanner.Text() == "" {
			break
		}

		tile.points = append(tile.points, []rune(scanner.Text()))
	}

	return n, &tile
}

func readInput(fname string) Input {
	input := NewInput()

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)

	for {
		n, tile := readTile(scanner)

		if tile == nil {
			break
		}

		input.tiles[n] = *tile
	}

	input.size = int(math.Sqrt(float64(len(input.tiles))))

	if input.size*input.size != len(input.tiles) {
		log.Fatal("invalid number of tiles")
	}

	input.keys = make([]int, 0, len(input.tiles))
	for k := range input.tiles {
		input.keys = append(input.keys, k)
	}

	// keeep the order predicatable
	sort.Ints(input.keys)

	return input
}
