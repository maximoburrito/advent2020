package main

var monster = [][]rune{
	[]rune("                  # "),
	[]rune("#    ##    ##    ###"),
	[]rune(" #  #  #  #  #  #   "),
}

func checkMonster(world [][]rune, row int, col int) bool {

	for i := 0; i < len(monster); i++ {
		for j := 0; j < len(monster[0]); j++ {
			if monster[i][j] == '#' {
				if world[row+i][col+j] != '#' {
					return false
				}
			}
		}
	}

	return true
}

func countHashes(world [][]rune) int {
	count := 0

	for i := 0; i < len(world); i++ {
		for j := 0; j < len(world[i]); j++ {
			if world[i][j] == '#' {
				count++
			}
		}
	}

	return count
}

func countMonsters(base [][]rune) int {
	// pp(base)

	worldSpots := countHashes(base)
	monsterSpots := countHashes(monster)

	for dir := 0; dir < 8; dir++ {
		monsters := 0
		world := orient(base, dir)

		for i := 0; i <= len(world)-len(monster); i++ {
			for j := 0; j <= len(world[0])-len(monster[0]); j++ {
				if checkMonster(world, i, j) {
					monsters++
				}
			}
		}

		if monsters > 0 {
			return worldSpots - monsterSpots*monsters
		}

		world = rotateRight(world)
	}

	return -1
}
