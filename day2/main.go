package main

// https://adventofcode.com/2020/day/2
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type PasswordSpec struct {
	Min      int
	Max      int
	Letter   rune
	Password string
}

func parseLine(line string) PasswordSpec {
	spec := PasswordSpec{}

	re := regexp.MustCompile(`^(\d+)-(\d+) (\w): (\w+)$`)
	matches := re.FindStringSubmatch(line)
	if len(matches) != 5 {
		log.Fatal("unable to parse %s", line)
	}

	n, err := strconv.Atoi(matches[1])
	if err != nil {
		log.Fatal(err)
	}
	spec.Min = n

	n, err = strconv.Atoi(matches[2])
	if err != nil {
		log.Fatal(err)
	}
	spec.Max = n

	spec.Letter = []rune(matches[3])[0]
	spec.Password = matches[4]

	return spec
}

func readInput() []PasswordSpec {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	specs := []PasswordSpec{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		spec := parseLine(scanner.Text())
		specs = append(specs, spec)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return specs
}

// ----------------------------------------

func test1(spec PasswordSpec) bool {
	n := 0

	for _, c := range spec.Password {
		if c == spec.Letter {
			n++
		}
	}

	return spec.Min <= n && n <= spec.Max
}

func part1(specs []PasswordSpec) int {
	count := 0

	for _, spec := range specs {
		if test1(spec) {
			count++
		}
	}

	return count
}

// ----------------------------------------

func test2(spec PasswordSpec) bool {
	chars := []rune(spec.Password)

	n := 0
	if chars[spec.Min-1] == spec.Letter {
		n++
	}
	if chars[spec.Max-1] == spec.Letter {
		n++
	}

	return n == 1
}

func part2(specs []PasswordSpec) int {
	count := 0

	for _, spec := range specs {
		if test2(spec) {
			count++
		}
	}

	return count
}

// ----------------------------------------

func main() {
	input := readInput()
	fmt.Println("Day 2")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
