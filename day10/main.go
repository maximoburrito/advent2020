package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func readInput(fname string) []int {
	nums := []int{}

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		n, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, n)
	}
	return nums
}

// ----------------------------------------
func part1(nums []int) int {
	prev := 0
	ones := 0
	threes := 1 // for the final jump
	for _, n := range nums {
		if n-prev == 1 {
			ones++
		} else if n-prev == 3 {
			threes++
		}

		prev = n
	}

	return ones * threes
}

// ----------------------------------------

func part2(nums []int) int {
	// prepend a 0 so we can better capture when
	// there are multiple connectors that can connect
	// from 0 jolts
	nums = append([]int{0}, nums...)

	// paths will record the number of paths we have from
	// plug i to the end
	paths := make([]int, len(nums))
	paths[len(nums)-1] = 1 // there's only one path from the last adaptor

	// now we compute all the paths starting from the
	// second to last adaptor
	for i := len(nums) - 2; i >= 0; i-- {
		// for adaptor i, the number of paths is simply the
		// sum of the paths of the adaptors we can connect to
		// (the ones within 3 jolts of us)
		for j := i + 1; j < len(nums) && nums[j]-nums[i] <= 3; j++ {
			// if we can connect from i to j, add those paths
			paths[i] += paths[j]
		}
	}

	// and finally, we return paths from the zero jolt node
	// we initially prepended....
	return paths[0]
}

// ----------------------------------------
func main() {
	input := readInput("input.txt")
	sort.Ints(input) // everybody wants it sorted

	fmt.Println("Day 10")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
