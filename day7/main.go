package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Rules probably should have been map[string][]Content
type Rules []Rule

type Rule struct {
	Color    string
	Contents []Content
}

type Content struct {
	Num   int
	Color string
}

// I like to write parser tools - need to find a golang library
// akin to instaparse. I hate writing this type of regexp/split
// hacked together parsers
func parseContent(text string) *Content {
	if text == "no other bags" {
		return nil
	}

	re := regexp.MustCompile(`(\d+) (.*) bag`)
	match := re.FindStringSubmatch(text)
	if len(match) != 3 {
		log.Fatal("can't parse", text)
	}

	num, err := strconv.Atoi(match[1])
	if err != nil {
		log.Fatal(err)
	}

	color := match[2]

	return &Content{num, color}
}

func parseRule(line string) Rule {
	rule := Rule{}

	re := regexp.MustCompile(`(.*) bags contain (.*)\.`)
	match := re.FindStringSubmatch(line)
	if len(match) != 3 {
		log.Fatal("can't parse", line)
	}

	rule.Color = match[1]
	for _, content := range strings.Split(match[2], ", ") {
		content := parseContent(content)
		if content != nil {
			rule.Contents = append(rule.Contents, *content)
		}
	}

	return rule
}

func readInput(fname string) Rules {
	rules := []Rule{}
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		rules = append(rules, parseRule(scanner.Text()))
	}

	return rules
}

// ----------------------------------------

// again, if Rules were map, this wouldn't be necessary
func findRule(rules Rules, color string) *Rule {
	for _, rule := range rules {
		if rule.Color == color {
			return &rule
		}
	}

	return nil
}

func contains(rules Rules, color string, target string) bool {
	rule := findRule(rules, color)
	if rule == nil {
		log.Fatal("can't find rule")
	}

	for _, content := range rule.Contents {
		if content.Color == target {
			return true
		}

		// this feels bad
		if contains(rules, content.Color, target) {
			return true
		}
	}

	return false
}

func part1(rules Rules) int {
	sum := 0

	for _, rule := range rules {
		if contains(rules, rule.Color, "shiny gold") {
			sum++
		}
	}

	return sum
}

// ----------------------------------------

func countInside(rules Rules, color string) int {
	rule := findRule(rules, color)
	if rule == nil {
		log.Fatal("can't find rule")
	}

	total := 0

	for _, content := range rule.Contents {
		innerBags := countInside(rules, content.Color)
		total += content.Num * (innerBags + 1)
	}

	return total
}

func part2(rules Rules) int {
	return countInside(rules, "shiny gold")
}

// ----------------------------------------
func main() {
	rules := readInput("input.txt")

	fmt.Println("Day 7")
	fmt.Println("part 1:", part1(rules))
	fmt.Println("part 2:", part2(rules))
}
