package main

func part1(input Input) int {
	game := Game(input)

	for game.winner() == 0 {
		game = game.simpleStep()
	}

	if game.winner() == 1 {
		return game.player1.score()
	} else {
		return game.player2.score()
	}
}
