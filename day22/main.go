package main

// uggh - this solution is very slow. Is it because the of the all the
// slice operations? Or deepequals? It doesn't seem like it should be
// so slow.  Lot's to learn. I hacked in a memo/cache onto part2
// before I realized I had two other large problems.  Not sure if it's
// neccessary. I have no idea why I needed the array copy (dup) - I
// can't see anything where I'm mutating the slives.

import "fmt"

func main() {
	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("Day 22")

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
