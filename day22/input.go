package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func mustInt(text string) int {
	n, err := strconv.Atoi(text)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

func readCards(scanner *bufio.Scanner) Cards {
	scanner.Scan() // player name
	ints := []int{}
	for scanner.Scan() {
		if scanner.Text() == "" {
			break
		}
		ints = append(ints, mustInt(scanner.Text()))

	}
	return ints
}

func readInput(fname string) Input {
	f, err := os.Open(fname)

	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)

	player1 := readCards(scanner)
	player2 := readCards(scanner)

	return Input{player1, player2}
}
