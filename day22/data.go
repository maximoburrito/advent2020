package main

import (
	"log"
	"reflect"
)

type Input Game

type Game struct {
	player1 Cards
	player2 Cards
}

type GameHistory struct {
	current  Game
	previous []Game
}

type Cards []int

func (cards Cards) score() int {
	total := 0
	for i, n := range cards {
		total += n * (len(cards) - i)
	}
	return total
}

func (h GameHistory) repeated() bool {
	for _, g := range h.previous {
		if reflect.DeepEqual(g, h.current) {
			return true
		}
	}
	return false
}

func (c Cards) wantsRecurse() bool {
	if len(c) == 0 {
		return false
	}

	if len(c) > c[0] {
		return true
	}

	return false
}

func dup(c Cards) []int {
	nc := make([]int, len(c))
	copy(nc, c)
	return nc
}

func (g Game) popToRecurse() Game {
	// XXX - without this dup, the slices get corrupted. why?
	return Game{
		dup(g.player1[1 : g.player1[0]+1]),
		dup(g.player2[1 : g.player2[0]+1]),
	}
}

func (g Game) winner() int {
	if len(g.player1) == 0 {
		return 2
	}
	if len(g.player2) == 0 {
		return 1
	}

	return 0
}

func (g Game) simpleStep() Game {
	if g.player1[0] > g.player2[0] {
		return g.applyWinner(1)
	} else {
		return g.applyWinner(2)
	}
}

func (g Game) applyWinner(winner int) Game {
	switch {
	case winner == 1:
		return Game{
			append(g.player1[1:], g.player1[0], g.player2[0]),
			g.player2[1:],
		}
	case winner == 2:
		return Game{
			g.player1[1:],
			append(g.player2[1:], g.player2[0], g.player1[0]),
		}
	}

	log.Fatal("unknown winner", winner)
	return Game{}
}

func (h GameHistory) next(g Game) GameHistory {
	return GameHistory{
		g,
		append(h.previous, h.current),
	}
}
