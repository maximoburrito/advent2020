package main

import (
	"fmt"
)

var gcount int
var gdepth int

type s struct {
	winner int
	score  int
}

var gseen map[string]s = map[string]s{}

func key(g Game) string {
	result := ""

	for _, n := range g.player1 {
		result = result + string(n) + ","
	}
	result = result + ":"
	for _, n := range g.player2 {
		result = result + string(n) + ","
	}

	return result
}

func playGame(_g Game) (int, int) {
	gkey := key(_g)
	r, ok := gseen[gkey]
	if ok {
		//fmt.Println("!!!! seen", _g)
		return r.winner, r.score
	}

	gcount++
	gdepth++

	defer func() { gdepth = gdepth - 1 }()

	if gcount%1000 == 0 {
		fmt.Println(gcount, gdepth, len(_g.player1), len(_g.player2))
	}
	//fmt.Println("=== starting game", _g)

	state := GameHistory{
		_g,
		[]Game{},
	}

	for {
		//fmt.Println("--", state.current)
		if len(state.current.player1) == 0 {
			//fmt.Println("win 2", state.current)
			gseen[gkey] = s{2, state.current.player2.score()}
			return 2, state.current.player2.score()
		}

		if len(state.current.player2) == 0 {
			//fmt.Println("win 1", state.current)
			gseen[gkey] = s{1, state.current.player1.score()}
			return 1, state.current.player1.score()
		}

		if state.repeated() {
			gseen[gkey] = s{1, state.current.player1.score()}
			//fmt.Println("REPEATED STATE", state.current)
			return 1, state.current.player1.score()
		}

		if state.current.player1.wantsRecurse() &&
			state.current.player2.wantsRecurse() {
			// fmt.Println("RECURSE", state.current, "->", state.current.popToRecurse())
			winner, _ := playGame(state.current.popToRecurse())
			state = state.next(state.current.applyWinner(winner))
		} else {
			state = state.next(state.current.simpleStep())
		}
	}
}

func part2(input Input) int {
	_, score := playGame(Game(input))
	return score
}
