package main

import (
	"container/ring"
	"fmt"
	"log"
)

type Cups []int

// ----------------------------------------

func makeInput(text string) Cups {
	cups := make([]int, 0, len(text))

	for _, c := range text {
		cups = append(cups, int(c)-'0')
	}

	return cups
}

func before(n int, ncups int) int {
	if n == 1 {
		return ncups
	}
	return n - 1
}

// ----------------------------------------

func contains(r *ring.Ring, n int) bool {
	found := false
	r.Do(func(p interface{}) {
		if p.(int) == n {
			found = true
		}
	})

	return found
}

func advanceTo(r *ring.Ring, target int) *ring.Ring {
	c := 0
	for r.Value.(int) != target {
		c++
		r = r.Next()
		if c > million {
			//showRing(r)
			log.Fatal("bad advance", target)
		}
	}
	return r
}

func stepRing(r *ring.Ring) *ring.Ring {
	saved := r
	rlen := r.Len() - 1

	cut := r.Unlink(3)

	head := r.Value.(int)
	target := before(head, rlen)
	for contains(cut, target) {
		target = before(target, rlen)
	}

	r = advanceTo(r, target)

	r.Link(cut)

	return saved.Next()
}

func stepRing2(r *ring.Ring, cache *[]*ring.Ring) *ring.Ring {
	saved := r
	rlen := million

	cut := r.Unlink(3)

	head := r.Value.(int)
	target := before(head, rlen)
	for contains(cut, target) {
		target = before(target, rlen)
	}

	//r = advanceTo(r, target)
	r = (*cache)[target]

	r.Link(cut)

	return saved.Next()
}

func showRing(r *ring.Ring) {
	fmt.Print("[ ")
	r.Do(func(p interface{}) {
		fmt.Print(p.(int), " ")
	})
	fmt.Println("]")
}

func part1(cups Cups) int {
	r := ring.New(len(cups))
	for i := 0; i < len(cups); i++ {
		r.Value = cups[i]
		r = r.Next()
	}

	for i := 0; i < 100; i++ {
		r = stepRing(r)
	}

	r = advanceTo(r, 1)
	result := 0
	r.Do(func(p interface{}) {
		if p.(int) != 1 {
			result = result*10 + p.(int)
		}
	})

	return result
}

const million = 1000000

func part2(cups Cups) int {
	r := ring.New(million)

	fast := make([]*ring.Ring, million+1) // zero is unused

	// initialize
	for i := 0; i < million; i++ {
		if i < len(cups) {
			r.Value = cups[i]
		} else {
			r.Value = i + 1
		}
		fast[r.Value.(int)] = r
		r = r.Next()
	}

	for i := 0; i < 10*million; i++ {
		r = stepRing2(r, &fast)
	}

	// got the solution
	r = fast[1].Next()
	n1 := r.Value.(int)
	r = r.Next()
	n2 := r.Value.(int)

	return n1 * n2
}

func main() {
	//input := makeInput("389125467")
	input := makeInput("459672813")
	//fmt.Println(input)

	fmt.Println("Day 22")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
