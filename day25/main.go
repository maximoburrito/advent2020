package main

import (
	"fmt"
)

type Input struct {
	pk1 int
	pk2 int
}


// The cryptographic handshake works like this:

//     The card transforms the subject number of 7 according to the
//     card's secret loop size. The result is called the card's public
//     key.
//
//     The door transforms the subject number of 7 according to the
//     door's secret loop size. The result is called the door's public
//     key.
//
//     The card and door use the wireless RFID signal to transmit the
//     two public keys (your puzzle input) to the other device. Now,
//     the card has the door's public key, and the door has the card's
//     public key. Because you can eavesdrop on the signal, you have
//     both public keys, but neither device's loop size.
//
//     The card transforms the subject number of the door's public key
//     according to the card's loop size. The result is the encryption
//     key.
//
//     The door transforms the subject number of the card's public key
//     according to the door's loop size. The result is the same
//     encryption key as the card calculated.



func determineLoopSize(pk int) int {
	loop := 0
	value := 1
	for value != pk  {
		loop ++
		value = (value * 7) % 20201227
		//fmt.Printf("%3d %10d\n",loop, value)
	}

	return loop
}


func transform(sn int, loops int) int {
	value := 1
	for i:=0;i<loops;i ++ {
		value = (value * sn) % 20201227
	}
	return value
}

func part1(input Input) int {
	//l1 := determineLoopSize(input.pk1)
	l2 := determineLoopSize(input.pk2)

	// fmt.Println("*", transform(input.pk1, l2))
	// fmt.Println("*", transform(input.pk2, l1))

	secret := transform(input.pk1, l2)

	//fmt.Println(l1,l2,secret)
	return secret
}

func main() {
	//input := Input{5764801, 17807724}
	input := Input{12090988, 240583}
	//fmt.Println(input)

	fmt.Println("Day 25")
	fmt.Println("part 1: ", part1(input))
}
