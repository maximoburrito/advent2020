package main

// I hate problems that come down to pure math tricks. Either you know
// the math or you don't.I would not have solved this if I hadn't been
// hinted to look up the track Maybe I should consider chinese
// remainder theorem a core CS algorithm, but it's far enough out for
// me that I wouldn't have been able to make any progress on this
// unless I found it.

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Input struct {
	start  int
	routes []int
	pos    []int
}

func parseRoutes(text string) ([]int, []int) {
	var routes []int
	var pos []int

	for i, route := range strings.Split(text, ",") {
		if route == "x" {
			continue
		}
		n, err := strconv.Atoi(route)
		if err != nil {
			log.Fatal("error parsing route", err)
		}
		routes = append(routes, n)
		pos = append(pos, i)
	}
	return routes, pos
}

func readInput(fname string) Input {
	var input Input
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)

	scanner.Scan()
	n, err := strconv.Atoi(scanner.Text())
	if err != nil {
		log.Fatal("error parsing timestamp", err)
	}
	input.start = n

	scanner.Scan()
	input.routes, input.pos = parseRoutes(scanner.Text())

	return input
}

// ----------------------------------------

func part1(input Input) int {
	// start at tine input time and check forward until
	// you find your target -yawn
	for time := input.start; ; time++ {
		for _, route := range input.routes {
			if time%route == 0 {
				return route * (time - input.start)
			}
		}

	}
	return -1
}

// ----------------------------------------
func mod(x int, y int) int {
	n := x % y
	if n < 0 {
		n += y
	}
	return n
}

// implement chinese remainder theorum - ugh
// this is messy, but I'm really not inclined
// to clean it up. Maybe later
func crm(nums []int, rems []int) int {
	answer := 0

	prod := 1
	for _, n := range nums {
		prod *= n
	}

	for i := 0; i < len(nums); i++ {
		n := prod / (nums[i])
		var u int
		for u = 0; mod(n*u, nums[i]) != 1; u++ {
		}

		part := rems[i] * n * u
		answer += part
	}
	return mod(answer, prod)
}

func part2(input Input) int {
	rems := make([]int, len(input.pos))
	for i := 0; i < len(rems); i++ {
		rems[i] = mod(input.routes[i]-input.pos[i], input.routes[i])
	}

	return crm(input.routes, rems)
}

// ----------------------------------------

// an alternate solution. I was thinking through this
// a bit more and although I would have never found
// the version of CRM I used in part2, I do think for
// just two inputs I would have easily come up with
// CRM involving two routes
func minicrm(x int, y int, rx int, ry int) int {
	for i := rx; ; i += x {
		if mod(i, x) == rx && mod(i, y) == ry {
			return i
		}
	}

	return -1
}

// I don't know that without serious pencil and paper time
// that I would have made the leap to combining the solutions,
// but I might have especially if I were playing with this
// in a repl as I would be if I were doing clojure this year
func part2b(input Input) int {
	x := input.routes[0]
	rx := input.pos[0]

	for i := 1; i < len(input.routes); i++ {
		y := input.routes[i]
		ry := mod(input.routes[i]-input.pos[i], input.routes[i])

		rx = minicrm(x, y, rx, ry)
		x = x * y
	}

	return rx
}

// ----------------------------------------
func main() {
	fmt.Println("Day 13")

	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))

	// leaving my partial work here - this
	// is the kind of thing I'd do in a repl
	// in clojure to explore the problem space
	// fmt.Println("*", minicrm(17,19,0,16))
	// fmt.Println("*", minicrm(13,19,11,16))
	// fmt.Println("*", minicrm(221,19,102,16))
	fmt.Println("part 2b:", part2b(input))
}
