package main

// I'm not terribly proud of this code. I really miss the fluidity of
// data and the ease of refactoring into small functions that I have
// in Clojure.  I've left my part2 solution as huge blob of code
// because, that's what felt natural for me to write in go. Every time
// I thought to extract code out to make it smaller, I felt the
// language was punishing me for it. I'm sure this is in large part a
// function both my lack of fluency in the language combined with my
// mental laziness to express the types and do things the go way.
//
// Other golang notes, I'm still not sure I'm doing a good job
// deciding when to pass a struct and when to pass a pointer to a
// struct.  I'm also not sure why "var cmatches map[int][]int" and
// "cmatches := map[int][]int{}" aren't interchangeable. I do see the
// difference, but it doesn't instinctively come to me.
//
// AoC note - ughh. This solution only works in the special case that
// the input has exactly one solution. I don't see that specified, but
// it was easy to see this property in the cmatches structure. And, of
// course, the problem didn't discuss how to choose a solution out of
// multiple solutions, so it was clear we shouldn't expect multiple
// soutions. I hate this kind of non-specifications where the your
// input is part of the problem specification and not just the
// ... input. Still, I'm at peace. AoC is puzzle competition that you
// solve with code and not a programming competition. Breathe
// in... Breathe out...

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Input struct {
	constraints []Constraint
	ticket      Ticket
	nearby      []Ticket
}

type Constraint struct {
	name string
	min1 int
	max1 int
	min2 int
	max2 int
}

type Ticket []int

func readConstraint(line string) Constraint {
	var constraint Constraint

	re := regexp.MustCompile(`([^:]+): (\d+)-(\d+) or (\d+)-(\d+)`)
	match := re.FindStringSubmatch(line)
	if len(match) != 6 {
		log.Fatal("can't parse", line)
	}

	constraint.name = match[1]

	n, err := strconv.Atoi(match[2])
	if err != nil {
		log.Fatal(err)
	}
	constraint.min1 = n

	n, err = strconv.Atoi(match[3])
	if err != nil {
		log.Fatal(err)
	}
	constraint.max1 = n

	n, err = strconv.Atoi(match[4])
	if err != nil {
		log.Fatal(err)
	}
	constraint.min2 = n

	n, err = strconv.Atoi(match[5])
	if err != nil {
		log.Fatal(err)
	}
	constraint.max2 = n

	return constraint
}

func readTicket(line string) Ticket {
	var ticket Ticket

	for _, text := range strings.Split(line, ",") {
		n, err := strconv.Atoi(text)
		if err != nil {
			log.Fatal(err)
		}
		ticket = append(ticket, n)
	}

	return ticket
}

func readInput(fname string) Input {
	var input Input

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		if text == "" {
			break
		}
		input.constraints = append(input.constraints, readConstraint(text))
	}

	scanner.Scan() // "your ticket:"
	scanner.Scan()
	input.ticket = readTicket(scanner.Text())

	scanner.Scan()
	scanner.Scan() //nearby tickets:
	for scanner.Scan() {
		ticket := readTicket(scanner.Text())
		input.nearby = append(input.nearby, ticket)
	}

	return input
}

// ----------------------------------------

func validForAnyConstraint(input Input, val int) bool {
	for _, constraint := range input.constraints {
		if constraint.min1 <= val && val <= constraint.max1 {
			return true
		}

		if constraint.min2 <= val && val <= constraint.max2 {
			return true
		}
	}

	return false
}

func part1(input Input) int {
	var result int

	for _, nearby := range input.nearby {
		for _, n := range nearby {
			if !validForAnyConstraint(input, n) {
				result += n
			}
		}
	}

	return result
}

// ----------------------------------------

func constraintMatches(constraint Constraint, val int) bool {
	if constraint.min1 <= val && val <= constraint.max1 {
		return true
	}

	if constraint.min2 <= val && val <= constraint.max2 {
		return true
	}
	return false
}

func isValid(input Input, ticket Ticket) bool {
	for _, n := range ticket {
		if !validForAnyConstraint(input, n) {
			return false
		}
	}
	return true
}

// this function is a beast
func part2(input Input) int {
	var valid []Ticket

	for _, nearby := range input.nearby {
		if isValid(input, nearby) {
			valid = append(valid, nearby)
		}
	}

	//var cmatches map[int][]int
	cmatches := map[int][]int{}

	for i, constraint := range input.constraints {
		for fnum := 0; fnum < len(input.ticket); fnum++ {
			matchesAll := true
			for _, ticket := range valid {
				if !constraintMatches(constraint, ticket[fnum]) {
					matchesAll = false
				}
			}
			if matchesAll {
				cmatches[i] = append(cmatches[i], fnum)
			}
		}
	}

	result := 1
	for len(cmatches) > 0 {
		//fmt.Println(cmatches)
		solved := -1
		for k, v := range cmatches {
			if len(v) == 1 {
				solved = k
			}
		}

		if solved == -1 {
			log.Fatal("non-unique solution at", cmatches)
		}

		//fmt.Println("solved", input.constraints[solved].name, "=", cmatches[solved])
		toRemove := cmatches[solved][0]
		delete(cmatches, solved)

		if strings.Contains(input.constraints[solved].name, "departure") {
			result *= input.ticket[toRemove]
		}

		for k, v := range cmatches {
			updated := []int{}
			for _, field := range v {
				if field != toRemove {
					updated = append(updated, field)
				}
			}
			cmatches[k] = updated
		}
	}

	return result
}

// ----------------------------------------

func main() {
	fmt.Println("Day 16")

	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("part 1", part1(input))
	fmt.Println("part 2", part2(input))
}
