package main

// The previous problem was big enough that I needed to split it
// into multiple files.  I'm feeling maybe I should have sone this
// here.
//
// I solved part 2 to solve part 1.  I assume that means I failed to
// see the more obvious solution for part 1.
//
// This code is quite a mess. I know how I'd split up the solution
// more cleanly in clojure. I'm getting better, but I'm still
// not feeling how to properly structure things. So much of this code
// is stuff that should be in a standard library somewhere....

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
)

type Input struct {
	labels []Label
}

type Label struct {
	ingredients []string
	allergens   []string
}

func readInput(fname string) Input {
	input := Input{}

	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if scanner.Text() != "" {
			input.labels = append(input.labels, readLabel(scanner.Text()))
		}
	}

	return input
}

func readLabel(line string) Label {
	re := regexp.MustCompile(`^(.+) \(contains (.+)\)$`)
	match := re.FindStringSubmatch(line)
	if len(match) != 3 {
		log.Fatal("parse error", line)
	}

	return Label{
		strings.Split(match[1], " "),
		strings.Split(match[2], ", "),
	}
}

func printInput(input Input) {
	for _, l := range input.labels {
		fmt.Println(l)
	}
}

func (input Input) allergens() []string {
	set := map[string]bool{}
	for _, l := range input.labels {
		for _, a := range l.allergens {
			set[a] = true
		}
	}

	allergens := make([]string, 0, len(set))
	for a := range set {
		allergens = append(allergens, a)
	}

	sort.Strings(allergens)
	return allergens
}

func (l Label) hasAllergen(target string) bool {
	for _, allergen := range l.allergens {
		if allergen == target {
			return true
		}
	}
	return false
}

func removeString(in []string, match string) []string {
	out := make([]string, 0, len(in))

	for _, text := range in {
		if text != match {
			out = append(out, text)
		}
	}

	return out
}

func (input Input) removeMatch(allergen string, ingredient string) Input {
	result := Input{make([]Label, 0, len(input.labels))}

	for _, l := range input.labels {

		label := Label{}
		label.allergens = removeString(l.allergens, allergen)
		label.ingredients = removeString(l.ingredients, ingredient)

		result.labels = append(result.labels, label)
	}

	return result
}

func findIntersect(acounts map[string]int, n int) []string {
	ingredients := []string{}

	for ingredient := range acounts {
		if acounts[ingredient] == n {
			ingredients = append(ingredients, ingredient)
		}
	}

	return ingredients
}

func part1(input Input) int {
	type solution struct {
		ingredient string
		allergen   string
	}

	for {
		allergens := input.allergens()
		if len(allergens) == 0 {
			break
		}

		solved := solution{}

		for _, a := range allergens {
			acounts := map[string]int{}
			seen := 0
			for _, l := range input.labels {

				if l.hasAllergen(a) {
					seen++
					for _, ingredient := range l.ingredients {
						acounts[ingredient] = acounts[ingredient] + 1
					}
				}
			}

			matched := findIntersect(acounts, seen)
			if len(matched) == 1 {
				solved = solution{matched[0], a}
			}
		}

		if solved.allergen == "" {
			log.Fatal("nothing solvable")
		}

		input = input.removeMatch(solved.allergen, solved.ingredient)
	}

	count := 0
	for _, l := range input.labels {
		count += len(l.ingredients)
	}

	return count
}

func formatPart2(dangerous map[string]string) string {
	allergens := []string{}
	for k := range dangerous {
		allergens = append(allergens, k)
	}
	sort.Strings(allergens)

	ingredients := []string{}
	for _, allergen := range allergens {
		ingredients = append(ingredients, dangerous[allergen])
	}

	return strings.Join(ingredients, ",")
}

func part2(input Input) string {

	type solution struct {
		ingredient string
		allergen   string
	}

	dangerous := map[string]string{}

	for {
		allergens := input.allergens()
		if len(allergens) == 0 {
			break
		}

		solved := solution{}

		for _, a := range allergens {
			acounts := map[string]int{}
			seen := 0
			for _, l := range input.labels {

				if l.hasAllergen(a) {
					seen++
					for _, ingredient := range l.ingredients {
						acounts[ingredient] = acounts[ingredient] + 1
					}
				}
			}

			matched := findIntersect(acounts, seen)
			if len(matched) == 1 {
				solved = solution{matched[0], a}
			}
		}

		dangerous[solved.allergen] = solved.ingredient
		input = input.removeMatch(solved.allergen, solved.ingredient)
	}

	return formatPart2(dangerous)
}

func main() {
	input := readInput("input.txt")

	//printInput(input)
	fmt.Println()

	fmt.Println("Day 21")
	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))

}
