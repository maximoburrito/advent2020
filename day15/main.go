package main

// I think this is the first AoC problem this season where the same code
// could be trivially reused for part1 and part2 with only a parameter tweak
//
// I'm unhappy with the solve function here, particularly in switching
// the sequence from the input numbers to the generated numbers.
//
// This code takes 2s to run. Is there a repeated sequence optimization or
// something?

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type Input []int

func readInput(fname string) Input {
	var input Input

	bytes, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatal(err)
	}

	line := strings.TrimSpace(string(bytes))
	for _, text := range strings.Split(line, ",") {
		n, err := strconv.Atoi(text)

		if err != nil {
			log.Fatal(err)
		}

		input = append(input, n)
	}

	return input
}

// ----------------------------------------

func solve(input Input, maxTurn int) int {
	history := map[int]int{}

	// no repeats in the initial set - so, easy.
	// don't consume the last of the input here
	for i := 0; i < len(input)-1; i++ {
		history[input[i]] = i
	}

	// save it as the "next" value
	next := input[len(input)-1]

	for i := len(input) - 1; i < maxTurn-1; i++ {
		prev, seen := history[next]

		history[next] = i
		if seen {
			next = i - prev
		} else {
			next = 0
		}
	}

	return next
}

func part1(input Input) int {
	return solve(input, 2020)
}

func part2(input Input) int {
	return solve(input, 30000000)
}

// ----------------------------------------

func main() {
	fmt.Println("Day 15")

	input := readInput("input.txt")
	//fmt.Println(input)

	fmt.Println("part 1:", part1(input))
	fmt.Println("part 2:", part2(input))
}
